SpotLight
=========

To show file slides into directories structure

```php

// Initialize where file slides are...
\Spotlight\Spotlight::init(['path' => /root/path/slides]);

// Output content of slides under 'section-name'
\Spotlight\Spotlight::show('section-name');


```
