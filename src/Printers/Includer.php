<?php

namespace Spotlight\Printers;

class Includer implements Printer
{

    public function out($filename)
    {
        if (is_file($filename)) {
            include $filename;
        }
    }
}
