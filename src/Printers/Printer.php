<?php

namespace Spotlight\Printers;

interface Printer
{
    public function out($path);
}
