<?php

namespace Spotlight;

use Spotlight\Printers\Printer;

class Slides
{

    private $printer;
    private $path;
    private $fileExtension;

    public function __construct(Printer $printer, $path = __DIR__, $fileExtension = '.html')
    {
        $this->printer       = $printer;
        $this->path          = $path;
        $this->fileExtension = $fileExtension;
    }

    public function show($name = null)
    {
        $slides = $this->listSlides($name);

        foreach ($slides as $file) {
            $this->printer->out($file);
        };
    }

    private function listSlides($name)
    {
        $filesystem = [];

        try {
            $filesystem = iterator_to_array(new \FilesystemIterator(
                $this->path . DIRECTORY_SEPARATOR . $name,
                \FilesystemIterator::CURRENT_AS_PATHNAME | \FilesystemIterator::SKIP_DOTS
            ));
            sort($filesystem);
        } catch (\UnexpectedValueException $exc) {

        }

        return $filesystem;
    }
}
