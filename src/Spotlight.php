<?php

namespace Spotlight;

use Spotlight\Printers\Includer;

class Spotlight
{

    /**
     * @var Slides
     */
    protected static $slide;

    public static function init(array $config = [])
    {
        if (!isset(self::$slide)) {
            $defaulConfig = [
                'path'      => __DIR__,
                'extension' => '.html'
            ];

            $config = array_merge($defaulConfig, $config);

            self::$slide = new Slides(new Includer, $config['path'], $config['extension']);
        }
    }

    public static function show($name = null)
    {
        return self::$slide->show($name);
    }
}
