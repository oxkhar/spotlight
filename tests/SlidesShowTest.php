<?php

namespace Tests\Spotlight;

/**
 *
 */
class SlidesTest extends \PHPUnit_Framework_TestCase
{

    protected $printer;
    protected $slides;
    protected $workingPath;
    protected $filesCreated;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->workingPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'slides';
        exec("mkdir -p " . $this->workingPath);

        $this->printer = $this->getMockBuilder(\Spotlight\Printers\Printer::class)
            ->setMethods(['out'])
            ->getMock();
        $this->slides  = new \Spotlight\Slides($this->printer, $this->workingPath);

        $this->filesCreated = array();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        exec("rm -rf " . $this->workingPath);
    }

    public function testIncludeSlideEspecifiedInParameter()
    {
        $fileSlide = $this->createSlide('portada');

        $this->printer->expects($this->once())->method('out')->with($fileSlide);

        $this->slides->show('portada');
    }

    public function testShowAllSlidesFromASectionWithOrder()
    {
        $fileSlide[] = $this->createSlide('portada', 50);
        $fileSlide[] = $this->createSlide('portada', 20);
        $fileSlide[] = $this->createSlide('portada', 40);
        $fileSlide[] = $this->createSlide('portada', 30);
        $fileSlide[] = $this->createSlide('portada', 10);

        sort($fileSlide);
        $arguments = array_map(function($file) {
            return [$file];
        }, $fileSlide);

        $this->printer
            ->expects($this->any())
            ->method('out')
            ->withConsecutive(...$arguments)
        ;

        $this->slides->show('portada');
    }

    public function testSlidesNotShowedWithEmptySections()
    {
        unlink($this->createSlide('section-empty', 50));

        $this->printer
            ->expects($this->never())
            ->method('out')
        ;

        $this->slides->show('section-empty');
    }

    protected function createSlide($name, $num = 0)
    {
        $code = sprintf('%06d', $num);
        $data = "$name $code";
        $path = $this->workingPath . DIRECTORY_SEPARATOR . $name;

        file_exists($path) || mkdir($path, 0777, true);

        $filename = $path . DIRECTORY_SEPARATOR . $code . "-" . $name . ".html";

        file_put_contents($filename, $data);

        $this->filesCreated[] = $filename;

        return $filename;
    }
}
